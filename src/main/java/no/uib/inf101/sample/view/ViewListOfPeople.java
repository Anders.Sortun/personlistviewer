package no.uib.inf101.sample.view;


import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;

import no.uib.inf101.sample.model.Person;
import no.uib.inf101.sample.model.PersonList;

public class ViewListOfPeople {
  
  public static void main(String[] args) {
    PersonList model = new PersonList();
    for (int i = 0; i < 10; i++) {
      model.addPerson(new Person("Adam", i));
      model.addPerson(new Person("Eva", i));
    }
    
    ViewListOfPeople view = new ViewListOfPeople(model);
    
    JFrame frame = new JFrame("MOCK");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view.mainPanel);
    frame.pack();
    frame.setVisible(true);
  }
  
  
  private PersonList model;
  private JScrollPane mainPanel;
  
  public ViewListOfPeople(PersonList model) {
    this.model = model;
    
    // Make a list of Persons and add them to the mainPanel
    DefaultListModel<Person> listModel = new DefaultListModel<>();
    JList<Person> list = new JList<>(listModel);
    for (Person person : model.getPersons()) {
      listModel.addElement(person);
    }
    
    this.mainPanel = new JScrollPane(list);
  }
  
  public JComponent getMainPanel() {
    return this.mainPanel;
  }
}
